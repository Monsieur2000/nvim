

-- L'option "buffer = 0" fait que le maping ne s'applique qu'au buffer courant
-- cela pour éviter que le mapping s'applique à un autre buffer ouvert qui n'est pas un fichier "help"
vim.keymap.set("n", "<CR>", "<C-]>", {buffer = 0, desc = "in help : press Enter to jump to the subject (topic) under the cursor."})
vim.keymap.set("n", "<BS>", "<C-T>", {buffer = 0, desc = "in help : press Backspace to return from the last jump."})

