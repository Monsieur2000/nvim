-- NOTE: pour que le frontmatter soit coloré syntaxiquement, ne pas oublier d'installer
-- le module yaml de TreeSitter

vim.opt.wrap = true
vim.opt.linebreak = true
vim.opt.spell = false

-- Autoupdate the keyword updated: in frontmatter when saving
local group = vim.api.nvim_create_augroup('VinceAuGroup', { clear = true })
vim.api.nvim_create_autocmd('BufWritePre', {
  command = "%s/^\\(updated: \\)\\d\\d\\d\\d-\\d\\d-\\d\\d/\\=submatch(1).strftime('%Y-%m-%d')/e",
  group = group,
  pattern = '*.md',
})

-- désactive les diagnostics
vim.diagnostic.enable(false)

-- Disable snacks indent
require('snacks').indent.disable()

-- On ajoute "b" (bold) pour entourer de double étoiles les mots
-- Pour l'italique on utilise _
require('mini.surround').setup {
  custom_surroundings = {
    b = {
      input = { '%*%*().-()%*%*' },
      output = { left = '**', right = '**' },
    },
  },
}

vim.cmd [[
  command! MergeBuffers execute 'lua MergeBuffers()'
]]

function MergeBuffers()
  local bufs = vim.fn.getbufinfo { bufloaded = 1 }
  local new_buf = vim.api.nvim_create_buf(true, false)
  for _, buf in ipairs(bufs) do
    local lines = vim.api.nvim_buf_get_lines(buf.bufnr, 0, -1, false)
    vim.api.nvim_buf_set_lines(new_buf, -1, -1, false, lines)
    vim.api.nvim_buf_set_lines(new_buf, -1, -1, false, { '\n' }) -- Add a new line between buffers
  end
  vim.api.nvim_set_current_buf(new_buf)
end

function CopyToPrompt()
  -- The name of the buffer to create or append to
  local buf_name = 'prompt.md'

  -- Get the selected text in visual mode
  local s_start = vim.fn.getpos "'<"
  local s_end = vim.fn.getpos "'>"
  local n_lines = math.abs(s_end[2] - s_start[2]) + 1
  local lines = vim.api.nvim_buf_get_lines(0, s_start[2] - 1, s_end[2], false)
  lines[1] = string.sub(lines[1], s_start[3], -1)
  if n_lines == 1 then
    lines[n_lines] = string.sub(lines[n_lines], 1, s_end[3] - s_start[3] + 1)
  else
    lines[n_lines] = string.sub(lines[n_lines], 1, s_end[3])
  end

  -- Try to find an existing buffer with the name prompt.md
  local buf = vim.fn.bufnr(buf_name)

  if buf == -1 then
    -- If the buffer doesn't exist, create a new buffer
    buf = vim.api.nvim_create_buf(true, false)
    vim.api.nvim_buf_set_name(buf, buf_name)
    -- Demande un texte à entrer au début du buffer
    local prompt = vim.fn.input 'Prompt: '
    vim.api.nvim_buf_set_lines(buf, -1, -1, false, { prompt })

    -- split la fenêtre pour afficher le buffer
    vim.api.nvim_command('vsplit ' .. buf_name)

    -- Ne bloque pas la fermeture de nvim si le buffer est pas sauvé
    vim.api.nvim_set_option_value('buftype', 'nofile', { buf = buf })

    vim.api.nvim_set_option_value('filetype', 'markdown', { buf = buf })
  end

  -- Append the selected text to the buffer
  vim.api.nvim_buf_set_lines(buf, -1, -1, false, { '' }) -- Add a newline before appending
  vim.api.nvim_buf_set_lines(buf, -1, -1, false, lines)
end

vim.api.nvim_set_keymap('v', '<leader>cp', ':<C-U>lua CopyToPrompt()<CR>', { noremap = true, silent = true }) -- <C-U> clear la ligne de commande. Je pense que c'est pour éviter des erreures avec le truc '<,'> qui apparait quand il y a une selection et qu'on ouvre la ligne de commande

-- Function to merge Markdown files based on links
local function merge_markdown_files()
  -- Get the current buffer's file path
  local current_file = vim.fn.expand '%:p'
  if not current_file or current_file == '' then
    print 'No file is currently open'
    return
  end

  -- Create a new buffer named 'merge.md'
  vim.cmd 'enew'
  vim.cmd 'file merge.md'
  local merge_buf = vim.api.nvim_get_current_buf()
  vim.api.nvim_buf_set_name(merge_buf, 'merge.md')
  vim.api.nvim_set_option_value('buftype', 'nofile', { buf = merge_buf }) -- Ne bloque pas la fermeture de nvim si le buffer est pas sauvé
  vim.api.nvim_buf_set_option(merge_buf, 'bufhidden', 'wipe') -- Optionally, hide the buffer when it’s deleted
  -- Incipit du nouveau document
  -- vim.api.nvim_buf_set_lines(merge_buf, 0, -1, false, { 'Merged Markdown Content:' })

  -- Helper function to strip YAML front matter
  local function strip_yaml_front_matter(content)
    local result = {}
    local in_front_matter = false
    for _, line in ipairs(content) do
      if line:match '^%-%-%-' then
        if in_front_matter then
          in_front_matter = false
        else
          in_front_matter = true
        end
        -- Skip this line
      elseif not in_front_matter then
        table.insert(result, line)
      end
    end
    return result
  end

  -- Helper function to append file content to the merge buffer
  local function append_file_content(file_path)
    if vim.fn.filereadable(file_path) == 1 then
      local file_content = vim.fn.readfile(file_path)
      local stripped_content = strip_yaml_front_matter(file_content)
      vim.api.nvim_buf_set_lines(merge_buf, -1, -1, false, stripped_content)
      vim.api.nvim_buf_set_lines(merge_buf, -1, -1, false, { '' }) -- Add an empty line for separation
    end
  end

  -- Read the current file's content and extract links
  local current_file_content = vim.fn.readfile(current_file)
  local link_pattern = '%[.-%]%((.-)%)'

  -- Add the current file's content to the merge buffer
  append_file_content(current_file)

  -- Extract links and process each one
  for _, line in ipairs(current_file_content) do
    for link in line:gmatch(link_pattern) do
      -- Only process if it's a relative link and not the current file
      if link:match '%.md$' and link ~= current_file then
        local link_file_path = vim.fn.fnamemodify(vim.fn.expand '%:p:h' .. '/' .. link, ':p')
        append_file_content(link_file_path)
      end
    end
  end

  -- Optionally, you can set this buffer to be displayed in a split
  vim.cmd 'wincmd p'
end

-- Map the function to a command for easy access
vim.api.nvim_create_user_command('MergeMarkdown', merge_markdown_files, {})

function InsertAuthorAndLicence()
  local row, col = unpack(vim.api.nvim_win_get_cursor(0))
  vim.api.nvim_buf_set_lines(0, row, row, false, {
    'author: ',
    'licence: CC BY-NC-SA 4.0',
  })
end

vim.api.nvim_create_user_command('InsertAuthorAndLicence', InsertAuthorAndLicence, {})
