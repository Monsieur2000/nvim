-- code from https://github.com/neovim/nvim-lspconfig/issues/662
-- tanks ret2src :-)

-- NOTE: On utilise le toggle du plugin snacks

-- Command to toggle inline diagnostics
-- vim.api.nvim_create_user_command('DiagnosticsToggleVirtualText', function()
--   local current_value = vim.diagnostic.config().virtual_text
--   if current_value then
--     vim.diagnostic.config { virtual_text = false }
--   else
--     vim.diagnostic.config { virtual_text = true }
--   end
-- end, {})

-- Command to toggle diagnostics
-- vim.api.nvim_create_user_command('DiagnosticsToggle', function()
--   local current_value = vim.diagnostic.is_enabled()
--   if current_value then
--     vim.diagnostic.enable(false)
--   else
--     vim.diagnostic.enable(true)
--   end
-- end, {})

-- vim.keymap.set('n', '<leader>tl', ':DiagnosticsToggleVirtualText<CR>', { desc = '[T]oggle in[L]ine diagnostics' })
