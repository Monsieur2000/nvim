local autocmd = vim.api.nvim_create_autocmd -- Create autocommand
local augroup = vim.api.nvim_create_augroup('vince_cmds', { clear = true }) -- Crée un groupe spéccifique auquel appartien les commandes perso

-- L'expliation des fonctions est indiquée dans le champs "desc"

autocmd('BufWritePre', {
  desc = 'Remove whitespace on save',
  pattern = '',
  group = augroup,
  command = ':%s/\\s\\+$//e',
})

autocmd('FocusLost', {
  desc = 'Sauvegarde quand la fenêtre perd le focus',
  pattern = '',
  group = augroup,
  command = ':wa',
})

autocmd('FileType', {
  pattern = { 'help', 'man' },
  group = augroup,
  desc = 'Use q to close the help or man window',
  command = 'nnoremap <buffer> q <cmd>quit<cr>',
})

autocmd('FileType', {
  pattern = 'qf',
  group = augroup,
  desc = 'Use q to close the quickfix or location list window',
  command = 'nnoremap <buffer> q <cmd>lclose<bar>cclose<cr>',
})
