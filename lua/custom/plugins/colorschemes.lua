----------------------------------------------------------------------
--              Ici on met les colorshemes qu'on aime               --
----------------------------------------------------------------------

--[[

INFO: le colorscheme par défaut est dans init.lua pour pas créer trop de divergence
avec la template de TJ de Vries. Ici on met ceux que l'on veut tester.
Ils sont chargés seulement si on appelle `Telescope colorscheme` avec le keymap
<leader>tcd

FIXME: J'aimerais bien ne pas avoir de keymap mais si je spécifie "Telescope colorscheme"
dans cmd, ça load pas le plugin.

--]]

-- Active les préviews quand on les séléctionne dans Telescope
-- Je sais pas si c'est le meilleure endroit pour mettre ça mais ça marche
-- NOTE: On utilise Snacks et c'est par défaut qu'il fait le préview
-- require "telescope".setup {
--   pickers = {
--     colorscheme = {
--       enable_preview = true
--     }
--   }
-- }

return {
  -- Tokyonight
  {
    'folke/tokyonight.nvim',
    lazy = true,
    cmd = 'Telescope colorscheme',
    keys = {
      { '<leader>tc', ':Telescope colorscheme<CR>', desc = 'colorscheme' },
      -- on peut l'écrire aussi comme ça { "<leader>tc", function() require('telescope.builtin').colorscheme() end, desc = "colorscheme"}
    },
    opts = {
      style = 'storm',
    },
  },

  -- Bluloco
  {
    'uloco/bluloco.nvim',
    lazy = true,
    cmd = 'Telescope colorscheme',
    keys = {

      { '<leader>tc', ':Telescope colorscheme<CR>', desc = 'colorscheme' },
    },
    dependencies = { 'rktjmp/lush.nvim' },
    config = function()
      vim.opt.termguicolors = true
    end,
    opts = {
      style = 'dark',
      italics = true,
    },
  },
  { 'catppuccin/nvim', lazy = true, name = 'catppuccin' },
  { 'EdenEast/nightfox.nvim', lazy = true },
  { 'Mofiqul/dracula.nvim', lazy = true }, -- mettre aussi option dans lualine
  -- {
  --   'olimorris/onedarkpro.nvim',
  --   lazy = true,
  -- },
}
