----------------------------------------------------------------------
--                       Interaction avec LLM                       --
----------------------------------------------------------------------

-- NOTE: Pour l'instant le plus simple et le plus cool. Manque juste un fonction
-- pour passer tous les buffers ouvert comme context.
--
-- Ya bien CodeCompanion qui fait ça mais il est trop compliqué

return {
  'David-Kunz/gen.nvim',
  cond = true,
  opts = {
    model = 'gemma2:2b', -- The default model to use.
    quit_map = 'q', -- set keymap for close the response window
    retry_map = '<c-r>', -- set keymap to re-send the current prompt
    accept_map = '<c-cr>', -- set keymap to replace the previous selection with the last result
    -- host = 'srvfjme-lab', -- The host running the Ollama service.
    host = 'localhost', -- The host running the Ollama service.
    port = '11434', -- The port on which the Ollama service is listening.
    display_mode = 'split', -- The display mode. Can be "float" or "split" or "horizontal-split".
    show_prompt = true, -- Shows the prompt submitted to Ollama.
    show_model = false, -- Displays which model you are using at the beginning of your chat session.
    no_auto_close = false, -- Never closes the window automatically.
    hidden = false, -- Hide the generation window (if true, will implicitly set `prompt.replace = true`)
    init = function(options)
      pcall(io.popen, 'ollama serve > /dev/null 2>&1 &')
    end,
    -- Function to initialize Ollama
    command = function(options)
      local body = { model = options.model, stream = true }
      return 'curl --silent --no-buffer -X POST http://' .. options.host .. ':' .. options.port .. '/api/chat -d $body'
    end,
    -- The command for the Ollama service. You can use placeholders $prompt, $model and $body (shellescaped).
    -- This can also be a command string.
    -- The executed command must return a JSON object with { response, context }
    -- (context property is optional).
    -- list_models = '<omitted lua function>', -- Retrieves a list of model names
    debug = false, -- Prints errors and the command which is run.
  },
  config = function(_, opts) -- On passe le tableau opts à la fonction pour qu'il soit accessible à setup(). Je sais pourquoi on doit mettre _, avant
    require('gen').setup(opts) -- nécessaire car on rajoute des autres trucs à faire, which key en l'occurence.
    require('which-key').add {
      { '<leader>a', group = 'I[A]', icon = '🤖' },
      { '<leader>aa', '<cmd>Gen Ask<cr>', desc = 'Ask AI' },
      { '<leader>aa', "<cmd>'<,'>Gen Ask<cr>", desc = 'Ask AI ', mode = 'v' },
      { '<leader>as', "<cmd>lua require('gen').select_model()<cr>", desc = 'Select LLM model' },
      { '<leader>ac', '<cmd>Gen Chat<cr>', desc = 'Chat AI' },
      { '<leader>ac', "<cmd>'<,'>Gen Chat<cr>", desc = 'Chat AI', mode = 'v' },
      { '<leader>ag', '<cmd>Gen<cr>', desc = 'Choose prompt' },
      { '<leader>ag', "<cmd>'<,'>Gen<cr>", desc = 'Choose prompt', mode = 'v' },
    }
    require('gen').prompts['Buf_Prompt'] = {
      prompt = '$text',
      replace = false,
    }
  end,
}
