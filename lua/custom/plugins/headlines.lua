-- Pour avoir les titres markdown en évidence
return {
  'lukas-reineke/headlines.nvim',
  cond = false,
  ft = 'markdown',
  dependencies = 'nvim-treesitter/nvim-treesitter',
  opts = {
    markdown = {
      bullets = {}, -- pour éviter les pictogramme comme dans les fichier org de emacs
      fat_headlines = false,
    },
  },
  config = true, -- or `opts = {}`
}
