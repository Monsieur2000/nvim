----------------------------------------------------------------------
--          Désactive les highlight de recherche quand on          --
--              commence à tapper ou bouger ailleurs               --
----------------------------------------------------------------------

return {
  'nvimdev/hlsearch.nvim',
  cond = false,
  event = 'BufRead',
  opts = {},
}
