----------------------------------------------------------------------
--            Création et update de Table des matières            --
----------------------------------------------------------------------

-- WARNING: il existe un formatter qui porte ce même nom et qui fait aussi le boulot
-- Voir ici : https://github.com/jonschlinkert/markdown-toc
-- Mais je sais pas trop comment on peut paramètrer des options

-- TODO: Checker comment ça se passe avec les hyperlien vers des caractères accentués,
-- Voir avec mkdnflow c'est lui qui gère leur interprétation

return {
  'hedyhli/markdown-toc.nvim',
  ft = 'markdown', -- Lazy load on markdown filetype
  cmd = { 'Mtoc' }, -- Or, lazy load on "Mtoc" command
  opts = {
    fences = {
      enabled = true,
      -- These fence texts are wrapped within "<!-- % -->", where the '%' is
      -- substituted with the text.
      start_text = 'toc:start',
      end_text = 'toc:end',
      -- An empty line is inserted on top and below the ToC list before the being
      -- wrapped with the fence texts, same as vim-markdown-toc.
    },
    toc_list = {
      markers = '-',
      indent_size = 4,
    },
    headings = {
      -- FIXME: J'aimerais bien pouvoir ne capturer que les titres de niveau 2 et 3, mais ces saloperies de lua pattern ne prennent pas en compte les {}
      pattern = '^(#+)%s+(.+)$', -- The first capture is for heading level ('###') and second is for the heading title
    },
  },
}
