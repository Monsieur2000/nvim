----------------------------------------------------------------------
--           Rendre le markdown plus joli en mode normal            --
----------------------------------------------------------------------

-- NOTE: 2025-01-25 : ce plugin semble être pas mal mais les config sont
-- difficiles à comprendre. Et c'est bordélique.
-- Pour l'instant, préférer MeanderingProgrammer/render-markdown.nvim
-- Il fonctionne comme on veut à peut près out of the box

return {
  'OXY2DEV/markview.nvim',
  cond = false,
  -- ft = 'markdown',
  opts = {
    preview = {
      hybrid_modes = { 'i' },
      enable_hybride_mode = true,
      modes = { 'n', 'no', 'c', 'v' },
      linewise_hybrid_mode = true,
    },
    markdown = {
      code_blocks = {
        label_direction = 'left',
      },
    },
  },
}
