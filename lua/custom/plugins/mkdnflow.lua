-- NOTE: pour le support de la coloration syntaxique, installer les modules TreeSitter :
-- markdown, markdown_inline, yaml et tous ceux pour la coloration de blocks de code

return {
  'jakewvincent/mkdnflow.nvim',
  cond = true, -- pour activer ou non non le plugin
  ft = 'markdown',
  opts = {
    modules = {
      yaml = true,
      -- cmp = true,
    },
    wrap = true,
    perspective = {
      priority = 'current',
      root_tell = '.git',
      fallback = 'first',
      nvim_wd_heel = false,
    },
    links = {
      conceal = false,
      transform_explicit = function(text)
        text = text:gsub(' ', '_')
        text = text:lower()
        return './' .. text
      end,
      -- Fonction pour corriger le fait que de nombreux lien vers une page sont comme ça (/page.md)
      -- et mkdnflow tente d'ouvrir un fichier de la racine du système et ça bug.
      transform_implicit = function(link)
        if link:match '^/' then
          return ('.' .. link)
        else
          return link
        end
      end,
    },
    new_file_template = {
      use_template = true,
      template = [[
---
title: {{ title }}
author: Monsieur2000
licence: CC BY-NC-SA 4.0
created: {{ date }}
updated: {{ date }}
---

# {{ title }}

]],
    },
    mappings = {
      MkdnEnter = { { 'i', 'n', 'v' }, '<CR>' }, -- This monolithic command has the aforementioned
      -- insert-mode-specific behavior and also will trigger row jumping in tables. Outside
      -- of lists and tables, it behaves as <CR> normally does.
      -- MkdnNewListItem = {'i', '<CR>'} -- Use this command instead if you only want <CR> in
      -- insert mode to add a new list item (and behave as usual outside of lists).
      MkdnFoldSection = { 'n', '<leader>fs' }, -- defaut mapping vont pas avec '<leader>ff' de recherche de fichier
      -- Pour unfolder <space><space> ça fonctionne, pas besoin d'utiliser la fonction idoine
    },
  },
}
