return {
  'nvim-neo-tree/neo-tree.nvim',
  enabled = false, -- on utilise celui de snacks
  branch = 'v3.x',
  dependencies = {
    'nvim-lua/plenary.nvim',
    'nvim-tree/nvim-web-devicons', -- not strictly required, but recommended
    'MunifTanjim/nui.nvim',
    -- "3rd/image.nvim", -- Optional image support in preview window: See `# Preview Mode` for more information
  },
  cmd = 'Neotree',
  keys = {
    -- {
    --   '<leader>e',
    --   function()
    --     require('neo-tree.command').execute { toggle = true, dir = vim.loop.cwd(), action = 'focus' } -- mettre action = 'show' si on veut pas que le focus se fasse sur neo-tree quand il s'ouvre.
    --   end,
    --   desc = '[E]xplorer NeoTree (cwd)',
    -- },
  },
  init = function()
    --[[
    INFO: le plugin se load que si on l'appelle par keymap ou par sa commande.
    Mais si on ouvre vim sur un dossier, on aimerait que `hijack_netrw_behavior` fonctionne.
    Il faut donc loader neotree au démarage si on ouvre vim sur un dossier.
    Cette fonction, qui est lue par Lazy au démarage car dans le champs `init`,
    Check si on est ouvert sur un dossier et load neotree direct si c'est le cas. Pour que
    `hijack_netrw_behavior` fonctionne.
    --]]

    if vim.fn.argc(-1) == 1 then
      local stat = vim.loop.fs_stat(vim.fn.argv(0))
      if stat and stat.type == 'directory' then
        require 'neo-tree'
      end
    end
  end,
  opts = {
    sources = { 'filesystem', 'buffers', 'git_status', 'document_symbols' },
    close_if_last_window = true,
    filesystem = {
      hijack_netrw_behavior = 'open_current',
      bind_to_cwd = false,
      follow_current_file = { enabled = true },
      use_libuv_file_watcher = true,
      filtered_items = {
        hide_dotfiles = false,
      },
      position = 'current',
    },
    default_component_configs = {
      indent = {
        with_expanders = true, -- if nil and file nesting is enabled, will enable expanders
        expander_collapsed = '',
        expander_expanded = '',
        expander_highlight = 'NeoTreeExpander',
      },
    },
    window = {
      mappings = {
        ['<space>'] = 'none',
        ['f'] = 'none',
      },
    },
  },
  config = function(_, opts) -- Je sais pas si c'est nécessaire de faire ça
    require('neo-tree').setup(opts)
  end,
}
