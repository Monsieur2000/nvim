----------------------------------------------------------------------
--                Crée des jolis cadre comme ceux-ci                --
----------------------------------------------------------------------

return {
    's1n7ax/nvim-comment-frame',
    dependencies = 'nvim-treesitter',
    keys = {
        {
            "<leader>cf",
            function()require('nvim-comment-frame').add_comment()end,
            desc = "Single line framed comment"
        },
        {
            "<leader>cm",
            function()require('nvim-comment-frame').add_multiline_comment()end,
            desc = "Multiline framed comment"
        },
    },
    opts = {
        disable_default_keymap = true,
    },
}
