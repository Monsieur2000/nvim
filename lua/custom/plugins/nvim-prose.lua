----------------------------------------------------------------------
--        Pour compter les mots d'un buffer et son temps de         --
--                             lecture                              --
----------------------------------------------------------------------

return {
  'skwee357/nvim-prose',
  ft = 'markdown',
  opts = true,
  config = function()
    require('nvim-prose').setup()

    vim.api.nvim_create_user_command('WordsCount', function()
      local word_per_pages = 280
      local wc = require('nvim-prose').word_count()
      local wc_num = tonumber(string.match(wc, '%d+'))
      local pages = math.floor(wc_num / word_per_pages)
      local reading_time = require('nvim-prose').reading_time()

      local output = string.format('%s %s %d pages\n', wc, reading_time, pages)
      vim.api.nvim_out_write(output)
    end, { desc = 'Compte le nombre de mot du fichier' })

    require('which-key').add {
      { '<leader>i', group = '[I]nfo', icon = '?' },
      {
        '<leader>iw',
        '<cmd>WordsCount<cr>',
        desc = 'Nb. mots',
      },
    }
  end,
}
