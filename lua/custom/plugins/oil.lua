return {
    'stevearc/oil.nvim',
    enabled = false,
    -- Optional dependencies
    dependencies = { "nvim-tree/nvim-web-devicons" },
    opts = {},
}
