----------------------------------------------------------------------
--                     Joli preview de markdown                     --
----------------------------------------------------------------------

-- NOTE: il existe aussi le plugin OXY2DEV/markview.nvim qui ressemble beaucoup
-- je le trouve un peu trop bordélique, 2025-01-25

return {
  'MeanderingProgrammer/render-markdown.nvim',
  cond = true,
  -- dependencies = { 'nvim-treesitter/nvim-treesitter', 'echasnovski/mini.nvim' }, -- if you use the mini.nvim suite
  -- dependencies = { 'nvim-treesitter/nvim-treesitter', 'echasnovski/mini.icons' }, -- if you use standalone mini plugins
  dependencies = { 'nvim-treesitter/nvim-treesitter', 'nvim-tree/nvim-web-devicons' }, -- if you prefer nvim-web-devicons
  opts = {
    render_modes = { 'n', 'v', 'i', 'c' },
    heading = {
      icons = { '󰲡  ', '󰲣  ', '󰲥  ', '󰲧  ', '󰲩  ', '󰲫  ' },
    },
  },
}
