-- NOTE: config pompée de LazyVim : https://www.lazyvim.org
return {
  'folke/todo-comments.nvim',
  dependencies = { 'nvim-lua/plenary.nvim' },
  cmd = { 'TodoTrouble', 'TodoTelescope' },
  event = { 'VimEnter' },
  opts = { signs = false },
  keys = {
    {
      ']t',
      function()
        require('todo-comments').jump_next()
      end,
      desc = 'Next todo comment',
    },
    {
      '[t',
      function()
        require('todo-comments').jump_prev()
      end,
      desc = 'Previous todo comment',
    },
    -- NOTE: on utilise le picker de snacks
    -- { '<leader>td', '<cmd>TodoTelescope<cr>', desc = 'Todo' },
    {
      '<leader>st',
      function()
        require('snacks').picker.todo_comments()
      end,
      desc = 'Todo',
    },
  },
}
