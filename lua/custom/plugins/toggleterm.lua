return {
  'akinsho/toggleterm.nvim',
  version = '*',
  enabled = false, -- on utilise snacks
  keys = {
    { '<leader>tt', '<cmd>ToggleTerm<cr>', desc = 'Toggle [T]erminal' },
  },
  opts = {
    direction = 'horizontal',
    size = 25,
    shell = '/usr/bin/zsh',
  },
}
