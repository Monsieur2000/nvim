----------------------------------------------------------------------
--         Permet de toggler des booléens ou switcher des          --
--         paramètres sur plusieurs lignes, idem pour les          --
--            fonctions. Et ça, juste avec une keymap !             --
----------------------------------------------------------------------

return {
    'ckolkey/ts-node-action',
    dependencies = { 'nvim-treesitter' },
    lazy = true,
    keys = {
        { "<leader>na", function()require("ts-node-action").node_action()end,  desc = "Trigger Node Action" }
    },
    opts = {},
}
