return {
    "folke/zen-mode.nvim",
    dependencies = {
        "folke/twilight.nvim"
    },
    cmd = 'ZenMode',
    opts = {
        -- your configuration comes here
        -- or leave it empty to use the default settings
        -- refer to the configuration section below
        plugins = {
            options = {
                showcmd = false,
            },
            alacritty = {
                enabled = false,
                font = "14", -- font size
            },
        }
    }
}
