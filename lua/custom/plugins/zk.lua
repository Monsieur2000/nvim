----------------------------------------------------------------------
--                           Zettlekasten                           --
----------------------------------------------------------------------

-- NOTE: installer le package zk de la distro. Cela installe le LSP zk par la même occasion.

return {
  'zk-org/zk-nvim',
  cond = true,
  ft = 'markdown',
  config = function()
    require('zk').setup {
      -- See Setup section below
    }

    local opts = { noremap = true, silent = false }

    require('which-key').add {
      { '<leader>z', group = '[Z]ettlekasten' },
    }

    vim.keymap.set('n', '<leader>zn', "<Cmd>ZkNew { title = vim.fn.input('Title: ') }<CR>", { desc = 'New Note' })

    vim.keymap.set('n', '<leader>zi', '<Cmd>ZkInsertLink<CR>', { desc = 'Insert link to Note' })

    vim.keymap.set('v', '<leader>zi', ':ZkInsertLinkAtSelection<CR>', { desc = 'Insert link on selection' }) -- NOTE: pas besoin de mettre '<,'> entre : et la commande, ils sont inséré automatiquement.

    -- Open the link under the caret.
    -- map("n", "<CR>", "<Cmd>lua vim.lsp.buf.definition()<CR>", opts)

    -- Create a new note after asking for its title.
    -- This overrides the global `<leader>zn` mapping to create the note in the same directory as the current buffer.
    -- map("n", "<leader>zn", "<Cmd>ZkNew { dir = vim.fn.expand('%:p:h'), title = vim.fn.input('Title: ') }<CR>", opts)
    -- Create a new note in the same directory as the current buffer, using the current selection for title.
    vim.keymap.set('v', '<leader>znt', ":'<,'>ZkNewFromTitleSelection { dir = vim.fn.expand('%:p:h') }<CR>", opts)
    -- Create a new note in the same directory as the current buffer, using the current selection for note content and asking for its title.
    vim.keymap.set('v', '<leader>znc', ":'<,'>ZkNewFromContentSelection { dir = vim.fn.expand('%:p:h'), title = vim.fn.input('Title: ') }<CR>", opts)

    -- Open notes linking to the current buffer.
    vim.keymap.set('n', '<leader>zb', '<Cmd>ZkBacklinks<CR>', opts)
    -- Open notes linked by the current buffer.
    vim.keymap.set('n', '<leader>fz', '<Cmd>ZkLinks<CR>', opts)

    -- Preview a linked note.
    vim.keymap.set('n', 'K', '<Cmd>lua vim.lsp.buf.hover()<CR>', opts)
    -- Open notes.
    vim.keymap.set('n', '<leader>zo', "<Cmd>ZkNotes { sort = { 'modified' } }<CR>", opts)
    -- Open notes associated with the selected tags.
    vim.keymap.set('n', '<leader>zt', '<Cmd>ZkTags<CR>', opts)
  end,
}
