----------------------------------------------------------------------
--                      Mappings et remappings                      --
----------------------------------------------------------------------

-- déplacer lignes entières
vim.keymap.set('n', '<A-j>', ':m .+1<CR>==')
vim.keymap.set('n', '<A-k>', ':m .-2<CR>==')
vim.keymap.set('i', '<A-j>', '<Esc>:m .+1<CR>==gi')
vim.keymap.set('i', '<A-k>', '<Esc>:m .-2<CR>==gi')
vim.keymap.set('v', '<A-j>', ":m '>+1<CR>gv=gv")
vim.keymap.set('v', '<A-k>', ":m '<-2<CR>gv=gv")

-- lors des recherches la ligne ciblée reste au milieu de l'écran
vim.keymap.set('n', 'n', 'nzzzv')
vim.keymap.set('n', 'N', 'Nzzzv')

-- se déplacer avec j et k dans les lignes wrappées
vim.keymap.set('n', 'j', 'gj')
vim.keymap.set('n', 'k', 'gk')

-- Space ouvre les "pliages de code mais les ferme pas. Avec ça, oui.
vim.keymap.set('n', '<space><space>', 'za')

-- Switcher vers le dernier buffer édité
vim.keymap.set('n', '<BS>', ':e #<CR>')

vim.keymap.set('i', 'jk', '<ESC>')

vim.keymap.set({ 'n', 'x' }, 's', '<Nop>') -- désactive la fonction de base de s qui supprime le caractère sous le curseur. Pour éviter le problème avec mini.surround

-- Pour éviter les erreur quand on lâche shift pas assez vite
vim.api.nvim_create_user_command('Q', 'q', {})
vim.api.nvim_create_user_command('WQ', 'wq', {})
vim.api.nvim_create_user_command('Wq', 'wq', {})

-- Orthographe
require('which-key').add {
  { '<leader>o', group = '[O]rthographe' },
  { '<leader>os', group = '[S]uggestions' },
  { '<leader>od', group = '[D]ictionaires' },
  -- NOTE: Toggle orthographe : on utilise Snacks.toggle
  -- {
  --   '<leader>to',
  --   mode = 'n',
  --   function()
  --     vim.opt.spell = not vim.opt.spell:get()
  --   end,
  --   desc = 'Toggle [O]rthographe',
  -- },
  { '<leader>ode', mode = 'n', '<cmd>lua vim.opt.spelllang = "en_us"<CR>', desc = 'Anglais' },
  { '<leader>odf', mode = 'n', '<cmd>lua vim.opt.spelllang = "fr"<CR>', desc = 'Français' },
  { '<leader>oda', mode = 'n', '<cmd>lua vim.opt.spelllang = "fr,en_us"<CR>', desc = 'Français-Anglais' },
  { '<leader>oo', mode = 'n', '1z=', desc = 'Corrige avec première Suggestion' },
  { '<leader>ol', mode = 'n', proxy = 'z=', desc = '[L]ist suggestions' },
}

-- Which-key groups
require('which-key').add {
  { '<leader>f', group = '[F]ind' },
}
