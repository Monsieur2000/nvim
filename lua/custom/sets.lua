-- INFO: Settings de base
--
-- Pas de swap files, persistance du undo après fermetur du fichier
vim.opt.swapfile = false
vim.opt.backup = false
vim.opt.undodir = os.getenv 'HOME' .. '/.vim/undodir'
vim.opt.undofile = true

vim.diagnostic.enable(true)
-- Orthographe
-- Si il y a des messages d'erreures sur le fait qu'il ne trouve pas de word list
-- créer un dossier .config/nvim/spell
vim.opt.spelllang = 'en_us,fr' -- choisi la/les langues
vim.opt.spell = false -- active la vérification
vim.cmd [[
  highlight SpellBad cterm=undercurl ctermfg=red guisp=red gui=undercurl
  highlight SpellCap cterm=underline ctermfg=yellow gui=undercurl guisp=yellow
  highlight SpellLocal cterm=underline ctermfg=blue gui=undercurl guisp=blue
]]

vim.opt.colorcolumn = '80' -- ligne de limite de longueure

vim.opt.relativenumber = true -- numérotation des lignes relatives

vim.o.shell = '/usr/bin/zsh'
